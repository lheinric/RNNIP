'''
trainNet.py

Script for training the different networks with enough functionality to
save the different hyperparameters.

There's an option parser that will run the script using these commands if this is run as the main
script, but the class can be loaded in as a module to interact more directly with the
training in a notebook.

Nicole Hartman, Winter 2018

'''

import numpy as np
import h5py
import os
import sys

from usefulFcts import *
from recurrent_attention_machine_translation import DenseAnnotationAttention

from keras import backend as K
from keras.models import Model, load_model
from keras.layers import Masking, Input, Dense, Dropout, LSTM, concatenate
from keras.layers import LSTMCell, GRU, GRUCell, Bidirectional, RNN
from keras.layers import BatchNormalization, Embedding, Lambda, TimeDistributed
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.utils.np_utils import to_categorical

import h5py
import sys

sys.path.append("..")


N_CATEGORIES=14
N_CLASSES = 4 # l, c, b, tau

class myModel():
    '''
    The basic base class for the models that I'm interested in training / comparing

    Should have the basic attributes for the LSTM, and the optimizer / fitting functions

    '''

    def __init__(self, nEpochs, nHidden, nDense, doEmbedding,
                 timeSteps=15, nFeatures=5, loadModel=False, modelDir='models',
                 dataTag=""):
        '''

  	    '''

        print("Initialize method from myModel()")

        self.nHidden = nHidden
        self.nEpochs = nEpochs
        self.nDense = nDense
        self.catTag = "_gradeEmbed" if doEmbedding else "" #"noEmbed"

        self.doEmbedding = doEmbedding
        self.timeSteps = timeSteps
        self.nFeatures = nFeatures
        self.loadModel = loadModel
        self.modelDir = modelDir
        self.dataTag = dataTag
        self.modelName = "please_overwrite_me"

        # Attibutes that should be overwritten in the children classes
        self.model = None
        self.hist = None
        self.monitor = None
        self.save_weights_only = None

    def createModel(self):
        '''
	    This method will be implemented by the children inheriting from this class
        '''
        pass

    def train(self, X_train, y_train_cat, weights_train, CV_split=0.2,
              nEpochs=50, loss_weights=None):
        '''
        Define the optimizer and train the model.

        It saves the best model to an hdf5 file specified by the self.modelName attribute.
        (Ideally, it would also be able to load in models to continue a previous training -
         still need to implement this)

        Inputs:
            X_train, y_train_cat: Inputs and truth labels (as one-hot vectors) for the train set
            weights_train: The sample weights from the pT reweighting
            CV_split: The percent of the data to hold out for the validation curves
            nEpochs: number of epochs to train for

        '''

        # Check that the inputs have the correct dimensionality
        _, ts, nFeat = X_train.shape
        if ts != self.timeSteps or nFeat != self.nFeatures:
            print("Error: X_train does not have the correct dimensionality for the model.")
            return

        # Check that the model has been properly instantiated
        if self.model is None:
            print("Error: This function should not be called from parent class myModel().")
            return

        # Define the optimizer
        self.model.compile(loss='categorical_crossentropy', optimizer='adam',
                           metrics=['acc'], loss_weights=loss_weights)

        # Get the relevant inputs
        if self.doEmbedding:
            print("Training with the embedding layer")
            my_input = [X_train[:,:,:-1],X_train[:,:,-1]]
        else:
            my_input = X_train

        mTag = '_weights.h5' if self.save_weights_only else '.hdf5'
        mLoc =  '{}/{}{}'.format(self.modelDir,self.modelName,mTag)
        print('model checkpoint: {}'.format(mLoc))

        monitor = self.monitor
        verbose = 2
        earlyStop = EarlyStopping(monitor=monitor, verbose=verbose, patience=20)
        #,restore_best_weights=False)
        mChkPt = ModelCheckpoint(mLoc,monitor=monitor, verbose=verbose,
                                 save_best_only=True,
                                 save_weights_only=self.save_weights_only)

	    # Fit the model
        self.hist = self.model.fit(my_input, y_train_cat,
                                   epochs=nEpochs, batch_size=256, verbose=0,
                                   validation_split=CV_split,
                                   callbacks=[earlyStop, mChkPt],
                                   sample_weight=weights_train)

		# Save the history object as well
        h = h5py.File('{}/{}_history.hdf5'.format(self.modelDir, self.modelName), 'w')

        # Save the weights as well!
        # This was before I started using the save_weights_only flag ModelCheckpoint
        #self.model.save_weights('{}/{}'.format(self.modelDir,weightFile))

        # Concatenate with the previous history object if you're training from a warm start
        if self.loadModel:
            print("Loading in the previous loss and acc training curves")

            assert loss_weights is None # Only implemented load_model for retraining w/o aux loss

            loss, val_loss, acc, val_acc = self.loadHistory()

            h.create_dataset("loss",     data=np.append(loss,    self.hist.history['loss']))
            h.create_dataset("val_loss", data=np.append(val_loss,self.hist.history['val_loss']))
            h.create_dataset("acc",      data=np.append(acc,     self.hist.history['acc']))
            h.create_dataset("val_acc",  data=np.append(val_acc, self.hist.history['val_acc']))

        else:

            for key, val in self.hist.history.items():
                h.create_dataset(key, data=val)

        print("Saving history object")
        h.close()

    def eval(self, X):
        '''
        Do the preprocessing of the inputs here since
        with the embedding layer and the functional api, I need to pass
        the inputs as two separate streams
        '''
        if self.doEmbedding:
            return self.model.predict([X[:,:,:-1],X[:,:,-1]], batch_size=256, verbose=0)

        else:
	        return self.model.predict(X, batch_size=256, verbose=0)


    def loadHistory(self):
        '''
        When you're retraining from a warm start, load in the previous training
        loss and accuracies and concatenate them with the new ones before saving
        the final history object.
        '''

        g = h5py.File("{}/{}_history.hdf5".format(self.modelDir, self.modelName),"r")

        loss     = g['loss'][:]
        val_loss = g['val_loss'][:]
        acc      = g['acc'][:]
        val_acc  = g['val_acc'][:]

        return loss, val_loss, acc, val_acc

    def processInputs(self):
        '''
        Process the separate trk stream inputs when doing the grade embedding
        '''
        cts_trk_inputs = Input(shape=(self.timeSteps,self.nFeatures-1),name="Continuous_trk_inputs")
        masked_input = Masking()(cts_trk_inputs)

        # Next look at a 2d embedding layer
        grade_input = Input(shape=(self.timeSteps,),name="Categorical_trk_inputs", dtype='int32')
        embed = Embedding(N_CATEGORIES+1,2,mask_zero=True,input_length=self.timeSteps)(grade_input)

        # Merge the two layers
        myMerge = concatenate([masked_input, embed])

        trkEmbeddedInputs = Model(inputs=[cts_trk_inputs,grade_input],outputs=myMerge)
        return trkEmbeddedInputs

    def get_lstm_out(self,x):
        '''
        When I return the lstm as a sequence, it's shape is
        (batch size, max # of timesteps, # hidden neurons)

        So this function just returns the last timestep of
        the sequence.
        '''

        return x[:,-1,:]

class myLSTM(myModel):

    '''
    Class for training the LSTM baselines
    '''

    def __init__(self, nEpochs=50, nHidden=50, nDense=10, doEmbedding=False,
                 timeSteps=15, nFeatures=5, loadModel=False,modelDir='models',
                 dataTag='', modelName=''):
        myModel.__init__(self, nEpochs, nHidden, nDense, doEmbedding, timeSteps,
                         nFeatures, loadModel, modelDir, dataTag)

        # Passing modelName as an input allows differing functionality from
        # training to testing as I'm developing
        if len(modelName) == 0:
            self.modelName = 'LSTM_{}'.format(self.modelTag())
        else:
            self.modelName = modelName

        self.monitor = 'val_acc'
        self.save_weights_only = False

        modelLoc = '{}/{}.hdf5'.format(modelDir,self.modelName)
        if self.loadModel and os.path.isfile(modelLoc):
            print("Loading previously trained LSTM")
            self.model = load_model(modelLoc)
            # Still need to load the history objects here as well

        else:
            print("Building the baseline LSTM")

            if self.doEmbedding:
                cts_trk_inputs = Input(shape=(timeSteps,nFeatures-1),name="Continuous_trk_inputs")
                grade_input = Input(shape=(timeSteps,),name="Categorical_trk_inputs", dtype='int32')
                trk_inputs=[cts_trk_inputs, grade_input]
                masked_input = self.processInputs()(trk_inputs)
            else:
                trk_inputs = Input(shape=(timeSteps,nFeatures),name="Trk_inputs")
                masked_input = Masking()(trk_inputs)

            # Feed this merged layer to an RNN
            lstm = LSTM(nHidden, return_sequences=False, name='LSTM')(masked_input)
            dpt = Dropout(0.2)(lstm)

            # Fully connected layer
            FC = Dense(nDense, activation='relu')(dpt)

            # Softmax for classification
            output = Dense(N_CLASSES, activation='softmax', name="Jet_class")(FC)
            self.model = Model(inputs=trk_inputs, outputs=output)

    def modelTag(self):
        '''
	    Get the tag for the model architecture using the class parameters that
        is common to all of the models that we're training
        '''

        nDense = self.nDense
        denseTag = '_{}dense'.format(nDense) if nDense != 0 else ''

        archTag = '{}units{}_{}{}'.format(self.nHidden, denseTag,
                                                 self.dataTag, self.catTag)

        return archTag

class LSTM_trkClass(myModel):

    '''
    Class for training the LSTM baselines
    '''

    def __init__(self, nEpochs=50, nHidden=50, nDense=10, doEmbedding=False,
                 timeSteps=15, nFeatures=5, loadModel=False,modelDir='models',
                 dataTag='', modelName='', nDenseTrk=0, alpha=0.5, nTrkClasses=5):
        myModel.__init__(self, nEpochs, nHidden, nDense, doEmbedding, timeSteps,
                         nFeatures, loadModel, modelDir, dataTag)

        self.nDenseTrk = nDenseTrk

        print("alpha=",alpha)

        # Passing modelName as an input allows differing functionality from
        # training to testing as I'm developing
        if len(modelName) == 0:
            trkClassTag = "_" if nTrkClasses==5 else "_{}_".format(nTrkClasses)
            self.modelName = 'LSTM{}trkClass_{}_alpha{:1.1f}'.format(trkClassTag,self.modelTag(),alpha)
        else:
            self.modelName = modelName

        self.monitor = 'val_Jet_class_acc'
        self.save_weights_only = True

        modelLoc = '{}/{}.hdf5'.format(modelDir,self.modelName)
        weightFile = modelDir + "/" + self.modelName + "_weights.h5"

        # atm, not supporting embedding for this class
        assert doEmbedding == False

        trk_inputs = Input(shape=(timeSteps,nFeatures),name="Trk_inputs")
        masked_input = Masking()(trk_inputs)

        # Feed this merged layer to an RNN
        lstm = LSTM(nHidden, return_sequences=True,name="LSTM")(masked_input)
        lstm_out = Lambda(self.get_lstm_out, output_shape=(nHidden,))(lstm)

        if nDenseTrk != 0:
            trk_vec = TimeDistributed(Dense(nDenseTrk))(lstm)
        else:
            trk_vec = lstm
        trk_out = TimeDistributed(Dense(nTrkClasses, activation='softmax'),name="Trk_class")(trk_vec)

        dpt = Dropout(0.2)(lstm_out)

        # Fully connected layer
        FC = Dense(nDense, activation='relu')(dpt)

        # Softmax for classification
        output = Dense(N_CLASSES, activation='softmax',name="Jet_class")(FC)
        self.model = Model(inputs=trk_inputs, outputs=[output,trk_out])


        if self.loadModel and os.path.isfile(weightFile):
            print("Loading weights for previously trained model")
            self.model.load_weights(weightFile)



    def modelTag(self):
        '''
	    Get the tag for the model architecture using the class parameters that
        is common to all of the models that we're training
        '''

        nDense = self.nDense
        denseTag = '_{}dense'.format(nDense) if nDense != 0 else ''

        archTag = '{}units{}_'.format(self.nHidden, denseTag)
        archTag += "{}trkDense_".format(self.nDenseTrk) if self.nDenseTrk != 0 else ""
        archTag += '{}{}'.format(self.dataTag, self.catTag)

        return archTag

    def eval(self, X, trk_out=False):
        '''
        Overwrite the parent class since this model has multiple outputs!

        Also, this class does not support embedding

        Inputs:
        - X: The test set to evaluate the model on
        - trk_out: The part model we want to evaluate the performance on
            * If False (default), look at the output of the jet classier, and
              output will have shape nJets, nJetClasses
            * If True, look at the output of the track classier, so the output
              will have shape nJets, timeSteps, nTrkClasses

        '''
        out = self.model.predict(X, batch_size=256, verbose=0)
        if trk_out:
            return out[1]
        else:
            return out[0]


class NMT(myModel):

    '''
    Class for training the NMT model for jointly aligning and translating for
    this classification problemself.

    There are several knobs I can turn on this model so I'll do my best to
    make it general enough to permit experimentation!

    '''

    def __init__(self, nEpochs=50, nHidden=50, nDense=10, doEmbedding=False,
                 timeSteps=15, nFeatures=5, loadModel=False,modelDir='models',
                 dataTag='', modelName='', encUnits=50,attnUnits=25):
        myModel.__init__(self, nEpochs, nHidden, nDense, doEmbedding, timeSteps,
                         nFeatures, loadModel, modelDir, dataTag)

        # Passing modelName as an input allows differing functionality from
        # training to testing as I'm developing
        if len(modelName) == 0:

            encTag = "enc_bidir_GRU_{}units".format(encUnits)
            decTag = "dec_{}attUnits_GRU_{}".format(attnUnits,self.modelTag())
            self.modelName = 'NMT_{}_{}'.format(encTag, decTag)

        else:
            self.modelName = modelName

        self.monitor = 'val_acc'
        self.save_weights_only = True

        assert not self.doEmbedding
        trk_inputs = Input((timeSteps,nFeatures), name="input_sequences")
        masked_inputs = Masking()(trk_inputs)

        encoder_rnn = Bidirectional(GRU(encUnits,
                                    return_sequences=True,
                                    return_state=True))
        x_enc, h_enc_fwd_final, h_enc_bkw_final = encoder_rnn(masked_inputs)

        # the final state of the backward-GRU (closest to the start of the input
        # sentence) is used to initialize the state of the decoder
        initial_state_gru = Dense(nHidden, activation='tanh')(h_enc_bkw_final)
        initial_attention_h = Lambda(lambda x: K.zeros_like(x)[:, 0, :])(x_enc)
        initial_state = [initial_state_gru, initial_attention_h]

        cell = DenseAnnotationAttention(cell=GRUCell(nHidden),
                                        units=attnUnits)
        # TODO output_mode="concatenate", see TODO(3)/A
        decoder_rnn = RNN(cell=cell, return_sequences=True, return_state=True)
        h1_and_state = decoder_rnn(masked_inputs, initial_state=initial_state, constants=x_enc)
        h1 = h1_and_state[0]

        lstm_out = Lambda(self.get_lstm_out, output_shape=(nHidden,))(h1)

        dpt = Dropout(0.2)(lstm_out)

        # Fully connected
        FC = Dense(nDense, activation='relu')(dpt)

        # Softmax for classification
        output = Dense(N_CLASSES, activation='softmax',name="Jet_class")(FC)

        self.model = Model(trk_inputs, output)

        weightFile = '{}/{}_weights.h5'.format(modelDir,self.modelName)
        if self.loadModel and os.path.isfile(weightFile):
            print("Loading weights for previously trained model")
            self.model.load_weights(weightFile)


    def modelTag(self):
        '''
	    Get the model tag defining this architecture
        '''

        nDense = self.nDense
        denseTag = '_{}dense'.format(nDense) if nDense != 0 else ''

        archTag = '{}units{}_{}{}'.format(self.nHidden, denseTag,
                                                self.dataTag, self.catTag)

        return archTag


if __name__ == '__main__':

    from argparse import ArgumentParser

    '''
    Load in the options from the command line
    '''
    p = ArgumentParser()

    p.add_argument('--nEpoch','--nEpochs', type=int, default=200, dest='nEpoch',
                   help = 'number of epochs (default 200)')
    p.add_argument('--nMaxTrack', type=int, default=15, dest="nMaxTrack",
                   help="Maximum number of tracks")
    p.add_argument('--nLSTMNodes', type=int, default=50, dest="nLSTMNodes",
                   help="number of hidden nodes for the LSTM algorithm")
    p.add_argument('--nFCNodes', type=int, default=10, dest="nFCNodes",
                   help="number of hidden nodes for the FC layer before classification")
    p.add_argument('--doEmbedding', action='store_true', dest="doEmbedding",
                   help="Whether you should embed the track category")
    p.add_argument('--nFixedTracks', '--nFixedTrks', type=int, default=-1, dest="nFixedTrks",
                   help="Whether you're only selecting jets with a fixed # of tracks")
    p.add_argument('--model', type=str, default='LSTM', dest="model",
                   help="Type of model: LSTM or LSTM_trkClass")

    # ---- extra args for the LSTM_trkClass model ----
    p.add_argument('--alpha', type=float, default=0.5, dest="alpha",
                   help="The weight to put on the trk classification loss: L = Ljet + alpha Ltrk (default alpha=0.5)")
    p.add_argument('--nDenseTrk', type=int, default=0, dest="nDenseTrk",
                   help="number of hidden nodes for an int layer from the LSTM seq -> trk classification")
    p.add_argument('--nTrkClasses', type=int, default=0, dest='nTrkClasses', help="# of trk classes")

    # ---- extra args for the NMT model ----
    p.add_argument('--nEncNodes', type=int, default=50, dest="nEncNodes",
                   help="number of hidden nodes for the encoder RNN")
    p.add_argument('--nDecNodes', type=int, default=50, dest="nDecNodes",
                   help="number of hidden nodes for the decoder RNN")
    p.add_argument('--nAttnNodes', type=int, default=25, dest="nAttnNodes",
                   help="number of nodes for the mlp getting the weights for the attention mechanism")


    p.add_argument('--loadModel', action='store_true', dest="loadModel",
                   help="Whether you retrain or load a previously trained model")
    p.add_argument('--nJets', type=int, default=3e6, dest="nJets",
                   help="Numer of jets in the dataset (default 3m)")

    p.add_argument('--dataTag', type=str, default='', dest='dataTag',
                   help="If you pass this argument, it will load in the file "\
                       +"'data_'+dataTag instead of using the other individual "\
                       +"arguments, and use this flag to name + save the trained model.")

    p.add_argument('--trkIPSign','--trkIPSgn', type=str, default='all', dest="trkIPSgn",
                   help="The sign of the IP to select for the tracks: all (default), pos / positive, or neg / negative")
    p.add_argument('--sortFlag', type=str, dest="sortFlag", default="sd0_rev",
                   help="Sorting configuration for the output dfs. Available options: sd0_rev (default), sd0_absrev, sd0_negrev, noSort")

    p.add_argument('--noNormVars', type=str, default='sd0,sz0,nNextToInnHits,nInnHits,'\
                   +'nsharedBLHits,nsplitBLHits,nsharedPixHits,nsplitPixHits,nsharedSCTHits',
                   help='Variables not to normalize: Pass -1 for an empty list')
    p.add_argument('--logNormVars', type=str, default='ptfrac, dr',
                   help='Variables to take the log of before normalizing, '\
                   +'default ptfrac, dr: Pass -1 for an empty list')
    p.add_argument('--jointNormVars', type=str, default='nPixHits,nSCTHits',
                   help='Variables to whiten: Pass -1 for an empty list')

    #p.add_argument('--modelDir', type=str, dest="modelDir", default="models",
    #               help="Directory for storing the models in (default: models)")

    p.add_argument("--mc", type=str, default="mc16d", dest="mc",
                   help="mc version: mc16a, mc16d (default), mc16e")
    p.add_argument('--jetCollection', type=str, default='Topo', dest="jetCollection",
                   help="Jet collection to train over: Topo (default) or PFlow")
    p.add_argument('--physicsSample', type=str, default='ttbar', dest="physicsSample",
                   help="the physics sample of interest: can be one of ttbar (default),"
                   +"Zprime_1.5TeV, Zprime_5TeV, hybrid_1.5TeV, hybrid_5TeV")

    args = p.parse_args()

    '''
    Step 1: Load in the training and test sets
    '''
    # Access the max length for the number of time steps for training the net
    jetTag = nJetsTag(args.nJets)
    if args.nFixedTrks == -1:
        jetTag += "_{}trks".format(args.nMaxTrack)
        timeSteps = args.nMaxTrack
    else:
        jetTag += "_ONLY_{}trks".format(args.nFixedTrks)
        timeSteps = args.nFixedTrks

    # Get the tag to select the number of jets and # of trks
    if len(args.dataTag) == 0:

        sortFlag = args.sortFlag
        ipTag = getIPTag(args.trkIPSgn)

        # Get a string representing the variables fed into this net
        noNormVars = strToList(args.noNormVars)
        logNormVars = strToList(args.logNormVars)
        jointNormVars = strToList(args.jointNormVars)

        varTag = "_".join(noNormVars)
        varTag += '_logNorm_' + "_".join(logNormVars) if len(logNormVars) != 0 else ""
        varTag += '_norm_' + "_".join(jointNormVars) if len(jointNormVars) != 0 else ""

        nFeatures = len(noNormVars + logNormVars + jointNormVars)

        dataTag = '{}_{}_{}{}'.format(jetTag, varTag, sortFlag, ipTag)

    else:
        dataTag = args.dataTag
        nFeatures=5 if 'grade' in dataTag else 17

    #mc = args.mc
    subDir = "{}_{}_{}".format(args.mc,args.jetCollection,args.physicsSample)

    #trkOrigTag = '_trkOrig' if args.model == 'LSTM_trkClass' else ''
    #if len(trkOrigTag) > 0:
    #    trkOrigTag += '' if nTrkClasses == 5 else "_{}trkClasses".format(nTrkClasses)

    nTrkClasses = args.nTrkClasses
    trkOrigTag = '_{}trkClasses'.format(nTrkClasses) if nTrkClasses != 0 else ''
    inputFile = 'data/{}/data_{}{}.hdf5'.format(subDir,dataTag,trkOrigTag)

    print("Attempting to open:",inputFile)
    f = h5py.File(inputFile,"r")

    # https://stackoverflow.com/questions/20928136/input-and-output-numpy-arrays-to-h5py
    X_train       = f['X_train'][:]
    y_train       = f['y_train'][:]
    ix_train      = f['ix_train'][:]
    weights_train = f['weights_train'][:]


    # Make the one-hot vectors
    y_train_cat = to_categorical(y_train, num_classes=N_CLASSES)

    '''
    Step 2: Load in the correct model
    '''
    modelDir = "models/" + subDir
    if args.model == "LSTM":
        print("Loading LSTM model")
        m = myLSTM(nHidden=args.nLSTMNodes, nDense=args.nFCNodes, doEmbedding=args.doEmbedding,
                   timeSteps=timeSteps, loadModel=args.loadModel, modelDir=modelDir,
                   nFeatures=nFeatures, dataTag=dataTag)

        my_output = y_train_cat
        my_weights = weights_train

    elif args.model == "LSTM_trkClass":


        m = LSTM_trkClass(nHidden=args.nLSTMNodes, nDense=args.nFCNodes,
                          doEmbedding=args.doEmbedding, timeSteps=timeSteps,
                          loadModel=args.loadModel, modelDir=modelDir,
                          nFeatures=nFeatures, dataTag=dataTag,
                          nDenseTrk=args.nDenseTrk, alpha=args.alpha,
                          nTrkClasses=nTrkClasses)

        y_trk_train = f['y_trk_train'][:]
        nTrks_train = f['nTrks_train'][:]
        my_output = [ y_train_cat, to_categorical(y_trk_train,num_classes=nTrkClasses) ]

        # Need to make sure we're not dividing by 0 for the trackless jets,
        # otherwise we'd get NaN losses + the track classifer model can't train
        trk_weights = weights_train.copy()
        trk_weights[nTrks_train != 0] /= nTrks_train[nTrks_train != 0]

        my_weights = [weights_train, trk_weights]

    elif args.model == "NMT":
        print("Loading NMT model")
        m = NMT(nHidden=args.nDecNodes, nDense=args.nFCNodes, doEmbedding=args.doEmbedding,
                timeSteps=timeSteps, loadModel=args.loadModel, modelDir=modelDir,
                nFeatures=nFeatures, dataTag=dataTag, encUnits=args.nEncNodes,
                attnUnits=args.nAttnNodes)

        my_output = y_train_cat
        my_weights = weights_train


    else:
        print("Error: {} is not a valid model".format(args.model))
        sys.exit()

    f.close()
    print(m.model.summary())

    '''
    Step 3: Train the corresponding model
    '''
    m.train(X_train, my_output, my_weights, nEpochs=args.nEpoch)

    print("Successfully trained model!")
