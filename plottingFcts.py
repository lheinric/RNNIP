from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib.colors import LogNorm
import h5py
from keras import backend as K

def trainingMetrics(modelName, modelDir='../models/mc16d'):

    '''
    Plot the training and val curves for a model.
    The history object is accessed using the model's name

    Inputs:
    - modelName: Allows you to save the training curves using the ! model name
    - subDir: If not an empty string, allows you to save the curves in a sub-directory
              inside figures.

    '''

    # Get the training loss / acc cuves
    g = h5py.File("{}/{}_history.hdf5".format(modelDir,modelName),"r")

    for key in g.keys():
        print(key)

    loss     = g['loss'][:]
    val_loss = g['val_loss'][:]

    epochs = np.arange(1,loss.size+1)

    plt.plot(epochs,loss,label='training')
    plt.plot(epochs,val_loss,label='validation')
    plt.xlabel('epochs',fontsize=14)
    plt.ylabel('cross-entropy loss',fontsize=14)
    plt.legend()
    subDir = modelDir[10:]
    plt.savefig('../figures/{}/loss_{}.pdf'.format(subDir,modelName))

    if 'alpha' not in modelName:


        acc      = g['acc'][:]
        val_acc  = g['val_acc'][:]

        plt.figure()
        plt.plot(epochs,acc,label='training')
        plt.plot(epochs,val_acc,label='validation')

        plt.xlabel('epochs',fontsize=14)
        plt.ylabel('accuracy',fontsize=14)
        plt.legend()
        plt.tight_layout()

        plt.savefig('../figures/{}/acc_{}.pdf'.format(subDir,modelName))

    else:
        # Plot the jet and track loss / acc functions

        for metric, ylabel in zip(['loss', 'acc'],
                                  ['cross-entropy loss','accuracy']):

            plt.figure()

            plt.plot(epochs,g['Jet_class_'+metric][:],label='jet train',
            color='C0',linestyle='-')
            plt.plot(epochs,g['val_Jet_class_'+metric][:],label='jet val',
            color='C1',linestyle='-')

            plt.plot(epochs,g['Trk_class_'+metric][:],label='trk train',
            color='C0',linestyle='--')
            plt.plot(epochs,g['val_Trk_class_'+metric][:],label='trk val',
            color='C1',linestyle='--')

            plt.xlabel('epochs',fontsize=14)
            plt.ylabel(ylabel,fontsize=14)
            plt.legend()
            plt.savefig('../figures/{}/jetTrk_{}_{}.pdf'.format(subDir,metric,modelName))

    plt.show()

    g.close()



def calculateEff(myHist):

    '''
    Given a histogram, calculate the area starting from the top
    and integrating to the bottomself.

    #, disregarding the lowest bin
    # so that the area never goes all the way to 1.

    Input:
        myHist: heights of the bins in a histogram

    Output:
        eff: Cummulative area of the histogram

    '''
    return np.add.accumulate(myHist[::-1]) / np.sum(myHist)


def sigBkgEff(m, X_test, y_test, ix_test=None, returnDisc=False, subDir='mc16d',
              tag='',fc = 0.07):

    '''
        Given a model, make the histograms of the model outputs to get the ROC curves.

        Input:
            m: A class (LSTM or attnModel) which are children of the myModel class
            X_test: Model inputs of the test set
            y_test: Truth labels for the test set
            returnDisc: If True, also return the raw discriminant (for comparison between
                        different evaluations for the RNNIPFlip studies).
            tag: Extra tag to append to the figure name for additional specificity
            fc: The amount by which to weight the c-jet prob in the disc. The
                default value of 0.07 corresponds to the fraction of c-jet bkg
                in ttbar.

        Output:
            effs: A list with 3 entries for the l, c, and b effs
            disc: b-tagging discriminant (will only be returned if returnDisc is True)
    '''

    # Evaluate the performance with the ROC curves!
    predictions = m.eval(X_test)

    # To make sure you're not discarding the b-values with high
    # discriminant values that you're good at classifying, use the
    # max from the distribution
    disc = np.log(np.divide(predictions[:,2], fc*predictions[:,1] + (1 - fc) * predictions[:,0]))

    discMax = np.max(disc)
    discMin = np.min(disc)

    myRange=(discMin,discMax)
    nBins = 200

    effs = []
    for output, flavor in zip([0,1,2], ['l','c','b']):

        ix = (y_test == output)

        # Plot the discriminant output
        nEntries, edges ,_ = plt.hist(disc[ix],alpha=0.5,label='{}-jets'.format(flavor),
                                      bins=nBins, range=myRange, density=True, log=True)

        # Calculate the baseline signal and bkg efficiencies
        eff = calculateEff(nEntries)
        effs.append(eff)

    plt.legend()
    plt.xlabel('$D = \ln [ p_b / (f_c p_c + (1- f_c)p_l ) ]$',fontsize=14)
    plt.ylabel('"Normalized" counts')
    plt.savefig('../figures/{}/disc_{}{}.pdf'.format(subDir, m.modelName, tag))
    plt.show()

    if returnDisc:
        return effs, disc
    else:
        return effs


def workingPoint(discriminant, nBins, myRange, WP=0.7):
    '''

    Given an output for a discriminant, look at where to cut to acheive given
    working point (WP), (whose default is set to 70%)

    Inputs:
        disciminant: The variable you want to cut on for the tagger
        WP: The desired efficiency of the cut
        nBins: # of bins for binning the histogram
        myRange:

    Output:
        cut: The value you should use to cut on the discriminant to acheive the
             efficeincy closest to the desired working point

    '''

    assert WP >= 0 and WP <= 1 #Error! The WP should be between 0 and 1

    # histogramize these values
    hist, edges = np.histogram(discriminant, bins=nBins, range=myRange, density=True)

    # Get the truth efficiencies from the histogramed discriminant
    teff = calculateEff(hist)

    # Find the index of the efficeincy hist corresponding to this WP
    i_eff = min((abs(eff-WP),j) for j,eff in enumerate(teff))[1]

    print('i_eff = {}, teff = {}'.format(i_eff,teff[i_eff]))

    # B/c of the way Ariel told me to cut on the ROC curves to eliminate
    # jets w/ 0 tracks, I need to subtract 1 to the idx location to get the correct cut.
    cut = edges[nBins-i_eff-1]

    print( "Cut on discriminant at {}".format(cut))

    return cut

def plotPtDependence(jetPts, y, discs, labels, WP=0.7, flat=False,
                     pt_edges=np.array([20, 50, 90, 150, 300, 1000]),
                     eff_bins = np.array([20,30,40,50,70,90,120,150,200,250,300,400,500,1000]),
                     tag='', subDir='mc16d',colors=None):
    '''
    For a given list of discriminants, plot the b-eff, l and c-rej as a function
    of jet pt.

    For a list of discriminants, plot the b-eff, l-rej, and c-rej (comparing
    different algs) as a function of the jet pt

    Inputs:
    - jetPts: The pt for each jet in the test (or dev) set
    - y: The truth lables:
         - 0: l
         - 1: c
         - 2: b
    - discs: A list of the discriminants
    - labels: The labels for the legend of this plot
    - WP: The working point for the inclusive signal efficiency,
    - flat: boolean to indicate whether to calcuate for flat signal eff
    - pt_edges: The edges for the pt bins
    - eff_bins: Bins for calculating the flat signal efficiency
    - tag: A tag to append to the model name
    - subDir: the subdirectory in figures to save the model too
    - colors: The colors to use for each of the discs in the histograms
              If None, python's default color scheme palatte will be used

    '''

    # Test that your inputs are the same length
    assert jetPts.size == discs[0].size and jetPts.size == y.size

    # Check that all the discs have appropriate legends
    assert len(discs) == len(labels)
    if colors is None:
        colors=["C{}".format(i) for i in range(len(discs))]

    # Get the edges for the histogram
    pt_midpts = (pt_edges[:-1] + pt_edges[1:]) / 2.
    bin_widths = (pt_edges[1:] - pt_edges[:-1]) / 2.
    Npts = pt_midpts.size

    for disc, label, color in zip(discs, labels, colors):

        # Get the sig eff and bkg rejections
        effs = [ np.zeros(Npts) ] * 3

        # Get the cut for the discriminant.
        nBins=1000
        myRange=(np.min(disc),np.max(disc))
        if flat:
            cut = getFlatSigEff(jetPts,eff_bins,y,disc,myRange,title=label,
                                 fig_idx=4 if label=='ip3d' else 5)
        else:
            cut = workingPoint(disc[y==2], nBins, myRange, WP=WP)

        for i in range(3):
            # Note for later: I could probably do this a lot more cleanly
            # with np.digitize
            for j, pt_min, pt_max in zip(np.arange(Npts), pt_edges[:-1], pt_edges[1:]):

                den_mask  = (jetPts > pt_min) & (jetPts < pt_max) & (y == i)
                num_mask  = den_mask & (disc > cut)
                effs[i][j] = num_mask.sum() / den_mask.sum()

            # For b-jets, plot the eff: for l and c-jets, look at the rej
            plt.figure(i+1)
            if i == 2:
                plt.errorbar(pt_midpts,effs[i],xerr=bin_widths,
                             color=color,fmt='.',label=label)
            else:
                plt.errorbar(pt_midpts,1/effs[i],xerr=bin_widths,
                             color=color,fmt='.',label=label)

    # Make the figures pretty + save them
    for i, flav in enumerate(['l','c','b']):

        metric = 'eff' if i==2 else 'rej'

        plt.figure(i+1)
        plt.xlabel('jet $p_T$ [GeV]',fontsize=14)
        plt.ylabel('{}-{}'.format(flav,metric),fontsize=14)
        plt.title('{}% WP'.format(int(WP*100)))
        plt.xlim(pt_edges[0],pt_edges[-1])
        plt.legend(loc='best',fontsize=12)

        if len(tag) > 0:
            plt.savefig('../figures/{}/{}-{}_vs_pT{}_{}.pdf'.format(subDir,flav,metric,'_flatEff' if flat else '',tag))
    plt.show()

def getFlatSigEff(pts,eff_bins,y,disc,disc_range,title='',fig_idx=4):
    '''
    Get the FLAT signal efficiency.

    '''

    indices = np.digitize(pts,bins=eff_bins)

    cut70_flat = np.array([workingPoint(disc[(y==2) & (indices==i)],200,disc_range)
                       for i in np.arange(1,len(eff_bins))])

    # Sanity check: Plot the signal efficiency for the WP + with smaller bins
    # to verify that I used small enough bins!
    yi = 2

    sig_eff1 = np.zeros(cut70_flat.shape)
    sig_eff2 = np.zeros(cut70_flat.shape[0]*2)



    for i, pt_min, pt_max, cut in zip(np.arange(eff_bins.shape[0]),
                                      eff_bins[:-1],eff_bins[1:],cut70_flat):

        den_mask = (pts > pt_min) & (pts < pt_max) & (y == yi)
        num_mask = den_mask & (disc > cut)
        sig_eff1[i] = num_mask.sum() / den_mask.sum()

        pt_mid = .5 * (pt_min + pt_max)

        den1_mask = (pts > pt_min) & (pts < pt_mid) & (y == yi)
        num1_mask = den_mask & (disc > cut)
        sig_eff2[i*2] = num_mask.sum() / den_mask.sum()

        den2_mask = (pts > pt_mid) & (pts < pt_max) & (y == yi)
        num2_mask = den_mask & (disc > cut)
        sig_eff2[i*2+1] = num_mask.sum() / den_mask.sum()


    midpts1 = .5 * (eff_bins[:-1]+eff_bins[1:])
    eff_bins2 = np.sort(np.concatenate((eff_bins,midpts1),axis=0))
    midpts2 = .5 * (eff_bins2[:-1]+eff_bins2[1:])

    plt.figure(fig_idx)
    plt.scatter(midpts1,sig_eff1,label='Eff at 70% WP')
    plt.scatter(midpts2,sig_eff2,label='Eff at 70% WP: smaller bins')
    plt.plot([0,eff_bins[-1]],[.7]*2,'k--')
    plt.xlabel('jet $p_T$ [GeV]')
    plt.ylabel('Signal eff')

    plt.ylim(0,1)
    plt.legend()
    plt.title(title)

    #plt.show()

    # What do I actually WANT to return though? The cut for each jet
    cuts = np.zeros_like(pts)
    # Ignore the overflow bin since it won't be on the plot anyways
    mask = (indices != cut70_flat.shape[0]+1)
    print(indices[mask].max())
    print(cut70_flat.shape)
    cuts[mask] = cut70_flat[indices[mask]-1]

    return cuts

def btagROC(beffs,leffs,ceffs,labels,title='',tag='',subDir='mc16d',
                colors=None, xmin=.6,ymax=1e3,legFontSize=10):
    '''
    Wrapper class around plotROC for the b-tagging roc curve that I make that
    overlay the b and c roc curves

    Inputs:
    - beffs: list of b-efficiencies
    - leffs: list of l-efficiencies
    - ceffs: list of c-efficiencies
    - labels: legend entries comparing the various studies
    Note: the above assume that all of these lists are the same length.
    The other inputs are the same as plotROC and are just passed directly to it.

    '''

    if colors is None:
        colors = ['C{}'.format(i) for i in range(len(beffs))]

    bkg_effs = leffs + ceffs
    myLabels = ["{} rej: {}".format(bkg, label) \
                for bkg in ['l','c'] for label in labels]

    myStyles = ['-']*len(beffs) + ['--']*len(beffs)

    plotROC(beffs*2, leffs+ceffs, myLabels, title=title, tag=tag, subDir=subDir,
            styles=myStyles, colors=colors*2, xmin=xmin, ymax=ymax,
            legFontSize=legFontSize)


def plotROC(teffs, beffs, labels, title='', tag='', subDir='mc16d',
            styles=None,colors=None, xmin=.6,ymax=1e3,legFontSize=10):
    '''
    Plot the ROC curves for a list of experiments you've run

    Inputs:
        teffs: List for the signal efficiencies
        beffs: List for the background efficiencies
        labels: List of identifiers for the experiment for the legend
        title: Title for the plot.
               If the default empty tag is passed, the plot will save some info
               about the datasample to the plot above the figure.
        tag: An option for the tag that you could append to the filename to save the plot
             The deault option of an empty tag won't produce any plots
        styles: If given, should be list of the same length as teff and beffs for the
                styles of the plots
        colors: If given, should be a list of colors to use for the plots

        xmin: The minimum value for the range on the x-axis
        ymin: The maximum value for the range on the y-axis
        legFontSize: The font size to use for the plot

    Note: This function expects the list arguments to all be the same length, but because
    of the way python's zip argument handles varying sized lists, if they aren't the same
    length it will zip to the shortest list.

    '''

    if styles is None:
        styles = ['-' for i in teffs]

    if colors is None:
        colors = ['C{}'.format(i) for i in range(len(teffs))]

    plt.figure()
    for teff, beff, label, style, color in zip(teffs, beffs, labels, styles, colors):

        plt.semilogy(teff, np.divide(1,beff), style, color=color, label=label)

    plt.xlabel('Signal Efficiency',fontsize=14)
    plt.ylabel('Background Rejection',fontsize=14)
    if len(title) > 0:
        plt.title(title)
    else:
        text="ATLAS Simulation Internal\n"
        text+=r"$\sqrt{s}$ = 13 TeV, mc16d $t\bar{t}$"
        plt.text(xmin,ymax,text, horizontalalignment='left', verticalalignment='bottom')


    # Set the axes to be the same as those used in the pub note
    plt.xlim(xmin,1)
    plt.ylim(1,ymax)
    plt.legend(loc='best',fontsize=legFontSize)

    if len(tag) != 0:
        plt.savefig('../figures/{}/roc_{}.pdf'.format(subDir,tag))

    plt.show()


def plotRatio(hist_arrs, kargs_arrs, xrange=None, normed=True, nBins=100,
              logY=True, rrange=None, xlabel='', ylabel='', title='',
              tag='', subDir=''):
    '''

    Given a list of arrays, overlay the histograms and compare the

    Inputs:
    - hist_arrs:  List of numpy arrays, where the first element of the list is the
                  histogram that we're comparing the others to
    - kargs_arrs: A list of the key word arguments that will be passed to each
                  histogram to be plotted
    - xrange: The range for the xaxis of the histograms
    - normed: Whether the histograms should be normalized
    - nBins: number of bins
    - logY: whether the yaxis for the overlaid histograms should be logarithmic
    - rrange: The range on the y-axis for the ratio panel

    - xlabel: Label for the x-axis
    - ylabel: Label for the y-axis
    - title:  Title for the figure

    - tag: If not an empty string, save the file in the ../figures
    - subDir: If this (and tag) is not an empty string, save the file in this subdirectory

    '''
    assert len(hist_arrs) == len(kargs_arrs) # input arrays must be the same length

    # Define the figure with two subplots of unequal sizes
    fig = plt.figure()
    gs = gridspec.GridSpec(7,1)
    ax1 = fig.add_subplot(gs[:6,0])
    ax2 = fig.add_subplot(gs[6:,0])

    for i, (arr, kargs) in enumerate(zip(hist_arrs, kargs_arrs)):

        # Plot the histgram
        n, bins, _ = ax1.hist(arr, range=xrange, bins=nBins, density=normed, log=logY, **kargs)

        if i == 0:
           nFirst = n
        if i > 0:
           ax2.plot((bins[:-1]+bins[:1])/2, n/nFirst, color=kargs['color'] )

    # Add a line indicating ratio=1
    ax2.plot(xrange,[1,1],'k--')
    if rrange is not None:
        ax2.set_ylim(rrange)

    # Add axes, titles and the legend
    ax1.set_ylabel('Arbitrary units')
    ax2.set_xlabel(xlabel,fontsize=14)
    ax2.set_ylabel('Ratio')
    ax1.set_title(title)
    ax1.legend()

    if len(tag) > 0:
        plt.savefig('../figures/{}/cf_{}.pdf'.format(subDir,tag))

    plt.show()


def plotInputs(X_list, y_list, dataLabels, figSubDir='mc16d/',
               trk_vars=['$s_{d0}$', '$s_{z0}$', '$p_T^{frac}$', '$\Delta R$', 'grade'],
               tag='', varRanges=[]):
    '''
    Given a list of inputs, plot the distribuitons of your normaliz
    Often times, for these studies, I've been comparing the impact of different
    datasets.

    Inputs:
    - X_list: A list of np arrays of shape nJets x nTrks x nTrkFeatures
    - y_list: A list of truth labels for the datasets in X_list
    - dataLabels: A
    - figSubDir: The subdirectory (inside figures) to save the models to
    - tag: If not an empty string, a tag for saving the models to
    '''

    defaultRanges=[(-10,10), (-10,10), (0,1), (0,8), (0,14)],
    nbins = 100
    pdgs = ['l','c','b']

    nJets, nTrks, nFeatures = X_list[0].shape
    nDatasets = len(X_list)

    if len(varRanges) == 0:
        varRanges = [defaultRanges for i in range(nDatasets)]

    fig = plt.figure(figsize=(4*nDatasets,2.5*nFeatures))

    # Loop through all of the options for the data inputs
    for i, (Xi, yi, dataLabel, var_ranges) in enumerate(zip(X_list, y_list, dataLabels,varRanges)):

        # Get the mask since all of the truth labels are track level variables
        y_trks = np.ones_like(Xi[:,:,0])
        y_trks = y_trks * yi.reshape(-1,1)

        mask_value = 0
        anti_mask = np.ones_like(Xi[:,:,0], dtype=bool)
        for feat in range(nFeatures):
            anti_mask = anti_mask & (Xi[:,:,feat] == 0)
        mask = ~ anti_mask

        y_trks = y_trks[mask]


        for j, ylabel, myRange in zip(range(nFeatures), trk_vars, var_ranges):

            idx = j*nDatasets+i+1
            ax = fig.add_subplot(nFeatures, nDatasets, idx)

            var_pdgs = [Xi[:,:,j][mask][y_trks == y] for y in range(4)]

            # Loop through the truth labels and make the histograms
            for var, l in zip(var_pdgs, pdgs):

                ax.hist(var, bins=nbins, range=myRange, histtype='step', density=True,
                        label="{}-jets".format(l))

                # Only use the log scale for the IPs
                if ('d0' in ylabel) or ('z0' in ylabel):
                    ax.set_yscale("log")

            if j == 0:
                plt.title(dataLabel)

            if i == 0:
                ax.set_ylabel(ylabel,fontsize=8)

            if (i == nDatasets-1) and (j == 0):
                ax.legend(loc='upper right',fontsize=14)

    if len(tag) > 0:
        plt.savefig("../figures/{}/inputs_{}.pdf".format(figSubDir,tag), bbox_inches='tight')

    plt.show()
