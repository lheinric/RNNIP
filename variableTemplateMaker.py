#!/usr/bin/env python3

'''
Once we have a trained python model, we'll use lwtnn to port it into athena!

To do this, I'll be using Dan's lwtnn package
	https://github.com/lwtnn/lwtnn
which wants
- The model architecture
- The model weights
- A variable template with the naming convention for the variables
   and the various scalings that we've applied to them!

The goal of this script is to go from the scaling_*.json file
that my root_to_numpy.py file spits out when it creates the Keras ready
hdf5 files, and put it in the format of the variable template that
the `kerasfunc2json.py` script expects!

Fortunately, the scaling*.json has an input string with '_' separated variables
corresponding to the variables the model is expecting.

There are three types of inputs
1. unnormalized inputs
2. Variables that we take the log of before normalizing
   When this string of variables starts, it will be preceeded by 'logNorm'
3. Variables that we just normalize regularly.

Plan: I'm going to name the variables the same thing for this output file,
except I'll add a 'log_' prefix for the variables that need to first be log
transformed.

'''
import json
from usefulFcts import strToList

def processInputStr(inputStr):
	'''

	Inputs:
	- inputStr

	Outputs:
	- noNormVars: The variables we're not normalizing
	- logNormVars: Variables that we're taking the log of before normalizing
	- jointNormVars: Variables we're normalizing directly

	'''

	myList =strToList(inputStr,"_")

	i_log  = myList.index('logNorm')
	i_norm = myList.index('norm')

	noNormVars = myList[:i_log]
	logNormVars = myList[i_log+1: i_norm]
	jointNormVars = myList[i_norm+1:]

	return noNormVars, logNormVars, jointNormVars



def writeTemplateFile(filename, noNormVars, logNormVars, jointNormVars,outputFile):
	'''
	Given the name of the name of the scaling file, lists of the variables for the
    different sorting schemes write the desired outputFile!
	'''

	# Dictionary mapping my variable names to the EDM names that Dan likes
    # Links that Dan sent me for EDM names:
	# https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/blob/master/BTagTrainingPreprocessing/src/BTaggingWriterConfiguration.hh#L236-246
	# https://gitlab.cern.ch/atlas/athena/blob/385ffc7800db42c7a3811f42c26dfdaf89034857/Event/xAOD/xAODTracking/Root/TrackSummaryAccessors_v1.cxx#L39-62
	EDM = { 'sd0':'IP3D_signed_d0_significance',
			'sz0':'IP3D_signed_z0_significance',
			'ptfrac':'ptfrac',
			'dr':'dr',
			'nInnHits':'numberOfInnermostPixelLayerHits',
			'nsharedBLHits':'numberOfInnermostPixelLayerSharedHits',
			'nsplitBLHits':'numberOfInnermostPixelLayerSplitHits',
			'nNextToInnHits':'numberOfNextToInnermostPixelLayerHits',
			'nPixHits':'numberOfPixelHits',
			'nPixHoles':'numberOfPixelHoles',
			'nsharedPixHits':'numberOfPixelSharedHits',
			'nsplitPixHits':'numberOfPixelSplitHits',
			'nSCTHits':'numberOfSCTHits',
			'nSCTHoles':'numberOfSCTHoles',
			'nsharedSCTHits':'numberOfSCTSharedHits'
          }


	# No track level inputs
	inputs = []

	seq_inputs = []
	trkVarList = []

	# Open the scaling file
	with open(filename, 'r') as varfile:
		varinfo = json.load(varfile)

	for v in noNormVars:
		trkVarList.append( {'name': EDM[v], 'scale':1, 'offset':0} )

	for v in logNormVars:

		m = varinfo[v]['mean']
		s = varinfo[v]['sd']

		d ={'name': 'log_{}'.format(EDM[v]), 'scale': m, 'offset':s}
		trkVarList.append(d)

	for v in jointNormVars:

		m = varinfo[v]['mean']
		s = varinfo[v]['sd']

		d ={'name': EDM[v], 'scale': m, 'offset':s}
		trkVarList.append(d)

	seq_input = {
		'name': 'Trk_Inputs',
		'variables': trkVarList
	}
	seq_inputs.append(seq_input)

	# Currently my models only have a single ouput.
	outputs = []
	output = {
		'name': 'Jet_class',
		'labels': ['p_{}'.format(x) for x in ['l','c','b','tau']]
	}
	outputs.append(output)

	out_dict = {
		'inputs': inputs,
 		'input_sequences': seq_inputs,
    	'outputs': outputs
	}

	# Dump the json file
	with open(outputFile, 'w') as varfile:
		json.dump(out_dict, varfile)



if __name__ == '__main__':

	from argparse import ArgumentParser
	'''
	Load in the options from the command line
	'''
	p = ArgumentParser()

	p.add_argument('--fileDir',type=str, dest='fileDir')
	p.add_argument('--scaleFile',type=str, dest='scaleFile')
	p.add_argument('--outputFile',type=str, dest='outputFile',
				   default="varTemplate.json")

	args = p.parse_args()

	filename = args.scaleFile
	inptStr = filename[16:-5]
	noNormVars, logNormVars, jointNormVars = processInputStr(inptStr)

	# Save the output file in a subdir of models
	# Extract the subdirectory from the fileDir (which starts with data/)
	outputDir = "models/" + args.fileDir[5:]
 
	writeTemplateFile(args.fileDir+"/"+filename,
					  noNormVars, logNormVars, jointNormVars,
					  outputDir+"/"+args.outputFile)
