'''
root_to_np.py

Go from root files to numpy arrays.
Apply any jet or track level cuts for the RNN studies.
Save Keras ready inputs for training.

Nicole Hartman, Fall 2018

'''

import numpy as np
import pandas as pd
from random import shuffle
from keras.preprocessing.sequence import pad_sequences
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import h5py

import glob
import os
import sys
import tqdm

from skhep.math.vectors import LorentzVector, Vector3D
import uproot
import xarray as xr

from usefulFcts import nJetsTag, getIPTag, strToList


def get_sort_index(arr, sort_type=None):
    '''
    Find the indices that sort an array

    Inputs:
    - arr: A 1d np.array that we're sorting by
    - sort_type: A string indicating what type of variables that we're sorting by
        - If 'abs' is in sort_type, it will use the abs of arr for the sort
        - If 'neg' is in sort_type, it will multiply the array by -1 for the sort
        - If 'rev' is in the string, the sort will be in descending order.

    Output:
    - indices: An np.array of ints of the same shape as arr

    '''

    if 'abs' in sort_type:
        indices = np.argsort( np.abs(arr) )
    elif 'neg' in sort_type:
        raise NotImplementedError
        #indices = np.argsort(-arr)
    else:
        indices = np.argsort(arr)

    if 'rev' in sort_type:
        return indices[::-1]
    else:
        return indices

def getdR(jeta, jphi, teta, tphi):
    '''
    Calculate dR, accounting for the 2pi difference b/w for phi b/w pi and -pi.

    Inputs:
    - jeta, jphi: (floats) for the jet axis direction
    - teta, tphi: (np.arrays) for the track dir

    Returns:
    - tdrs: An np array for the opening angles b/w the tracks and the jet axis.

    '''

    deta = teta - jeta

    # Calculate delta phi, accounting the for the 2pi at the boundary
    dphi = tphi - jphi
    dphi[dphi >  np.pi] = dphi[dphi >  np.pi] - 2*np.pi
    dphi[dphi < -np.pi] = dphi[dphi < -np.pi] + 2*np.pi

    tdrs = np.sqrt( deta**2 + dphi**2)
    return tdrs

def passJet(jet_arrays, trk_arrays, ievt, ijet):
    '''
    Figure out whether a given jet passes the standard cuts.
    Returns True if the jet passes the standard cuts, and false
    otherwise.

    Input:
    - jet_arrays: Uproot arrays for the jet vars
    - trk_arrays: Uproot arrays for the trk vars
    - ievt: Event number
    - ijet: Jet number

    '''

    # The variables used for processing the array
    cut_jet_pt  = 20000 # in [MeV] (at this point)
    cut_jet_eta = 2.5
    cut_JVT = 0.59

    # jet pt cut
    if jet_arrays[b"jet_pt"][ievt][ijet] < cut_jet_pt:
        return False

    # jet eta cut
    if np.abs(jet_arrays[b"jet_eta"][ievt][ijet]) > cut_jet_eta:
        return False

    # Electron overlap removal
    if not jet_arrays[b'jet_aliveAfterOR'][ievt][ijet]:
        return False

    # Muon overlap removal
    if not jet_arrays[b'jet_aliveAfterORmu'][ievt][ijet]:
        return False

    # JVT cut
    if (jet_arrays[b'jet_pt'][ievt][ijet] < 60000) & \
        ((np.abs(jet_arrays[b'jet_eta'][ievt][ijet]) < 2.4)) & \
        (jet_arrays[b'jet_JVT'][ievt][ijet] < cut_JVT):
        return False

    if len(trk_arrays[b'jet_trk_ip3d_grade'][ievt][ijet]) == 0:
        return False

    return True

def getData(filename="/gpfs/slac/atlas/fs1/d/rafaeltl/public/RNNIP/FTAG_ntups/"\
                     +"user.rateixei.mc16_13TeV.410470."\
                     +"PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.AOD."\
                     +".dijetSamplesNominal20180629_Akt4EMTo/"\
                     +"user.rateixei.14594351.Akt4EMTo._000*.root",
            nJets=200000, jetCollection="Topo", sort_flag="sd0_rev"
           ):
    '''
    Open the root files processed by Rafael, access the desired jet and trk
    variables, apply the jet level cuts, implement the desired track sort,
    and return the lists for the desired jet and track variables of
    length nJets.

    Jet Cuts::# WARNING:
        - Calibrated jet pT > 20 GeV, |eta| < 2.5
        - JVT > 0.59 central, low pT jets
        - Jet can't overlap with a previously IDed photon
    - Also rescale pTs from MeV -> GeV

    Inputs:
    - filename: Directory to and name of the root file
    - nJets: Max # of events. (The default is 200k)
    - jetCollection: the jet collection to use - Topo (default) or PFlow
    - sort_flag: The variable specifying what we're sorting by

    Outputs:
    - output_jet_array: A list of the corresponding jet variables
    - output_trk_array: A list of lists for the trk variables
    - jet_vars: The variable names of the jet variables from the Ntuple
    - trk_vars: The variable names of the trk variables from the Ntuple

    '''

    # Below are the jet and trk variables for accessing the events out of the root file
    jet_vars = ["jet_LabDr_HadF","jet_pt_orig","jet_eta_orig","jet_phi_orig","jet_m",
                "jet_ip3d_llr","jet_ip3d_pb","jet_ip3d_pc","jet_ip3d_pu","jet_ip3d_ntrk",
                #"jet_mv2c10",
                "jet_aliveAfterOR","jet_aliveAfterORmu",
                "jet_JVT","jet_pt","jet_eta",
                "jet_rnnip_pu","jet_rnnip_pc","jet_rnnip_pb","jet_rnnip_ptau"] #,"jet_bH_Lxy"]

    trk_vars  = ["jet_trk_ip3d_d0sig", "jet_trk_ip3d_z0sig",
                 #"jet_trk_ip3d_d0", "jet_trk_ip3d_z0",
                 "jet_trk_ip3d_grade", "jet_trk_ip3d_llr",
                 "jet_trk_pt", "jet_trk_eta", "jet_trk_theta", "jet_trk_phi",
                 "jet_trk_orig", #"jet_trk_algo",
                 "jet_trk_nNextToInnHits",
                 "jet_trk_nInnHits",
                 "jet_trk_nsharedBLHits",
                 "jet_trk_nsplitBLHits",
                 "jet_trk_nPixHits",
                 "jet_trk_nPixHoles",
                 "jet_trk_nsharedPixHits",
                 "jet_trk_nsplitPixHits",
                 "jet_trk_nSCTHits",
                 "jet_trk_nSCTHoles",
                 "jet_trk_nsharedSCTHits",
                ]

    print('Running over EM4{} jets'.format(jetCollection))

    # Declare the output arrays that we're going to put stuff into?
    output_jet_array = []
    derived_trk_vars = ['jet_trk_ptfrac','jet_trk_dr',
                        'jet_trk_prbP','jet_trk_pPerp']
    # Each entry here should be a list of lists to use Keras's pad_sequences function
    output_trk_array = {k:[] for k in trk_vars + derived_trk_vars}


    # Some constants needed in the TCT derived variables definitions
    trkMinPtCut = 700 # MeV at this point
    coeffPt = 10.
    mpi = 139.57 # pion mass (MeV)


    # Determine the sort_var from the sort_flag
    if "sd0" in sort_flag:
        sort_var = b"jet_trk_ip3d_d0sig"
    elif "sz0" in sort_flag:
        sort_var = b"jet_trk_ip3d_z0sig"
    elif "pt" in sort_flag:
        sort_var = b"jet_trk_pt"
    else:
        print("Error: No supported sorting variable for sort_flag `{}` in getData().".format(sort_flag))
        return

    print("Sorting by {}".format(sort_var))

    # Open the file and get the np arrays
    for fname in glob.glob( filename ):

        print("Opening file {}".format(fname))

        # Access the ttree
        myTree = uproot.open(fname)["bTag_AntiKt4EM{}Jets".format(jetCollection)]

        # Select the jet and trk arrays
        jet_arrays = myTree.arrays(jet_vars)
        trk_arrays = myTree.arrays(trk_vars)

        ## Event Loop
        n_events = len(jet_arrays[b"jet_pt"])

        print('n_events',n_events)

        for ievt in range(n_events):

            if len(output_jet_array) % 10000 == 0:
                print('    Jet {} / {}'.format(len(output_jet_array), int(nJets)))

            # Jet loop
            n_jets = len(jet_arrays[b"jet_pt"][ievt])
            for ijet in range(n_jets):

                if not passJet(jet_arrays, trk_arrays, ievt, ijet):
                    continue

                # Append to a list for variables of the jet class
                # The encode() function just lets us go from a string to as bytes literal
                # https://stackoverflow.com/questions/45360480/is-there-a-formatted-byte-string-literal-in-python-3-6
                print(jvar)

                jetList = [jet_arrays[jvar.encode()][ievt][ijet] \
                           for jvar in jet_vars if jvar != 'jet_bH_Lxy']

                # The jet_bH_Lxy is a list of the Lxys for b-hadrons in the jet
                # (i.e, we have two b-hadrons for a double b-tagger), so we need
                # to extract this variable differently
                #jetList = jetList + [jet_arrays[b'jet_bH_Lxy'][ievt][ijet][0]]

                output_jet_array.append(np.array(jetList).reshape(1,-1))

                # Get the trk variable mask for this jet
                ip3d_mask = (np.array(trk_arrays[b'jet_trk_ip3d_grade'][ievt][ijet]) != -10)
                index_list = get_sort_index( np.array(trk_arrays[sort_var][ievt][ijet])[ip3d_mask],
                                             sort_type=sort_flag )

                for tvar in trk_vars:
                    # Do the IP3D track selection + sort
                    output_trk_array[tvar].append( np.array(trk_arrays[tvar.encode()][ievt][ijet])[ip3d_mask][index_list] )

                # Now calculate the dr and ptfrac variables
                dr = getdR(jet_arrays[b'jet_eta_orig'][ievt][ijet],
                           jet_arrays[b'jet_phi_orig'][ievt][ijet],
                           output_trk_array['jet_trk_eta'][-1],
                           output_trk_array['jet_trk_phi'][-1])

                ptFrac = output_trk_array['jet_trk_pt'][-1] / jet_arrays[b'jet_pt_orig'][ievt][ijet]

                output_trk_array['jet_trk_dr'].append(dr)
                output_trk_array['jet_trk_ptfrac'].append(ptFrac)

                # TCT variables: Add the prbP and p_perp variables that Vadim was using
                pfrac = (output_trk_array['jet_trk_pt'][-1] - trkMinPtCut) / np.sqrt(jet_arrays[b'jet_pt_orig'][ievt][ijet])
                prbP = pfrac / (coeffPt + pfrac)
                output_trk_array['jet_trk_prbP'].append(prbP)

                jet = LorentzVector()
                jet.setptetaphim(jet_arrays[b'jet_pt_orig'][ievt][ijet],
                                 jet_arrays[b'jet_eta_orig'][ievt][ijet],
                                 jet_arrays[b'jet_phi_orig'][ievt][ijet],
                                 jet_arrays[b'jet_m'][ievt][ijet])
                jvec = jet.vector
                jdir = jvec / jvec.mag

                pPerp = []
                for tpt, teta, tphi in zip(output_trk_array['jet_trk_pt'][-1],
                                           output_trk_array['jet_trk_eta'][-1],
                                           output_trk_array['jet_trk_phi'][-1]):

                    trk = LorentzVector()
                    trk.setptetaphim(tpt, teta, tphi, mpi)
                    tvec = trk.vector
                    t_perp = (tvec.cross(jdir)).mag

                output_trk_array['jet_trk_pPerp'].append(np.array(pPerp))


                # Return if desired
                if len(output_jet_array) >= nJets:
                    return output_jet_array, output_trk_array, \
                           jet_vars, trk_vars+derived_trk_vars

def saveData(output_jet_array, output_trk_array, jetVars, trkVars, nTrks,
             subDir, sort_flag, save=True):
    '''

    Take lists of the arrays for the jet and trk variables,
    truncates and pads the arrays to fixed length, creates a
    df for the jets and an xarray for the tracks, and saves
    them for future analysis.


    Inputs:
    - output_jet_array: A list of np arrays for the jet variables extracted
                        from the Ntuple
    - output_trk_array: A list of 2d np arrays for the track n_variables
    - jetVars: The names for the jet leafs we extracted from the Ntuple
    - trkVars: Names of the trk leafs we extracted from the Ntuple
    - nTrks: The number of tracks to truncate / pad the sequences to
    - subDir: Folder inside data/ to save the output to
    - sort_flag: The flag to append to the datafile to represent the sorting
                 scheme used
    - save: (default True) whether to save the df + xr files or not

    Outputs:
    - jet_df: A df for the jet variables where the rows represent different jets
              and the columns represent the jet variables.
    - trk_xr: An xarray for the track variables. Note: trk_xr.values is an
              np.array of shape (nJets, nTrks, nTrkVariables)
    '''

    # Concatenate lists to get numpy arrays
    jet_np = np.concatenate(output_jet_array,axis=0)
    print("jet_np",jet_np.shape)

    # Need to truncate / pad to consistent # of jets before concatenating
    trk_vars_flat = []
    for v in trkVars:
        padded = pad_sequences(output_trk_array[v], maxlen=nTrks,
                               padding='post', truncating='post',dtype=np.float32)
        trk_vars_flat.append(padded.reshape(-1,nTrks,1))

        print(v,trk_vars_flat[-1].shape)

    trk_np = np.concatenate(trk_vars_flat,axis=2)
    print("trk_np",trk_np.shape)

    # Put the jet np array in a df.
    # Since the "jet_" prefix is redundant, don't bother putting it in
    jet_cols = [c[4:] for c in jetVars]
    jet_df = pd.DataFrame(jet_np, columns=jet_cols)

    # Put the trk np array in an xarray
    # Since the "jet_trk_" prefix is redundant, don't bother putting it in
    trk_cols = [c[8:] for c in trkVars]
    # Additionally: rename a couple of the variables
    # ip3d_d0sig -> sd0
    # ip3d_z0sig -> sz0
    trk_cols[0] = "sd0"
    trk_cols[1] = "sz0"

    trk_xr = xr.DataArray(trk_np,
                          coords=[('jet',np.arange(len(output_jet_array))),
                                  ('trk',np.arange(nTrks)),
                                  ('var',trk_cols)])

    # Rescale the pTs from MeV -> GeV
    MeVtoGeV = lambda x: x*0.001

    jPtCols = [v for v in ['pt','pt_orig','m'] if v in jet_cols]
    jet_df[jPtCols] = jet_df[jPtCols].apply(MeVtoGeV)

    for v in ['pt','pPerp']:
        if v in trk_cols:
            trk_xr.loc[:,:,v] = MeVtoGeV(trk_xr.loc[:,:,v])


    # Save the df + xarray
    if save:
        data_tag = "{}_{}trks_{}".format(nJetsTag(len(output_jet_array)),nTrks,sort_flag)
        jet_df.to_hdf('data/{}/jet_{}.h5'.format(subDir,data_tag), key='jet_df',mode='w')
        trk_xr.to_netcdf('data/{}/trk_{}.nc'.format(subDir,data_tag))

    return jet_df, trk_xr


def loadData(filename, nJets, sort_flag="sd0_rev"):
    '''
    Load in the df and xarray object saved from getData, and implement an
    alternate sorting (if desired)

    Inputs:
    - filename: The name of the .h5 or .nc files that we're trying to process
    - nJets: The number of jets to put inside the new file
    - sort_flag: The sorting flag if you want to resort what is in the df

    '''

    # Step 0: Extract the name + path to the file from the full file name + path
    name = filename.split('/')[-1]
    path = '/'.join(filename.split('/')[:-1])

    # Step 1: Unpack the filename to access the parameters used for this file
    # Ignorming the last three chars just lets us ignore the .h5 at the end
    strList = name[:-3].split('_')
    i = 1

    # Get the previous jet string
    prev_jetTag = strList[i]
    i += 1

    # Get the number of tracks used for padding and truncating previously
    if strList[i] == "ONLY":
        # And don't use the last 3 characters of the string which say trks
        prev_nTrks = int(strList[i+1][:-4])
        i += 2
    else:
        prev_nTrks = int(strList[i][:-4])
        i += 1

    # Access the sort flag used to make the file originally
    prev_sort_flag = "_".join(strList[i:])

    # Step 2: Open the files
    data_tag = name[4:-3]
    jet_df = pd.read_hdf('{}/jet_{}.h5'.format(path,data_tag), key='jet_df')
    trk_xr = xr.open_dataarray('{}/trk_{}.nc'.format(path,data_tag))

    # Select the requested number of jets
    if prev_jetTag != nJetsTag(nJets):
        jet_df = jet_df[:nJets]
        trk_xr = trk_xr[:nJets]

    # Step 3: Resort the tracks (if desired)
    if sort_flag == prev_sort_flag:
        print("The requested {} sort was already implemented".format(sort_flag))

    elif sort_flag != "noSort":

        # We need to manipulate the trk dataframe now.
        # Don't I lose some of the power by going back to np arrays here?
        X = trk_xr.values

        # The neg flag means multiply the IPs by -1
        if "neg" in sort_flag:
            # Recall, sd0 and sz0 are the first trk features)
            # This also works for trackless jets b/c -1 * 0 = 0 will still be masked
            X[:,:,0] *= -1
            X[:,:,1] *= -1

        if "abs" in sort_flag:
            raise NotImplementedError

        # There are two different ways I could implement this, using Michela's
        # or Michael's sorting functions

        sortVar = sort_flag.split("_")[0]
        rev = ("rev" in sort_flag)
        trkSort(X, trk_xr.indices['var'], sort=sortVar, rev=rev, mask_value=0.)

    else:
        trk_xr = scrambleTrks(trk_xr)
    # Or alternatively, I could add the "noSort" flag to Michael's function
    # to scramble the tracks

    # Idea: If it's a different sorting, might as well save the dfs here(?)
    # (Depending on how long the sorting takes)

    return jet_df, trk_xr

def trkSelection(jet_df, trk_df, nFixedTrks=-1, trkIPSgn="all"):
    '''

    Can either select jets with a fixed number of tracks, or do
    the IP sign selection.

    Jet Cuts:
        - If nFixedTrks != -1, select trks w/ this many tracks

    Trk Cuts:
        - If trkIPSgn is not all, selects the sign for the trk IP
          based on the flag passed by trkIPSgn

    Inputs:
        jet_df: df with the jet variables
        trk_df: df with the trk variables
        nFixedTrks: If we're only selecting jets with a fixed # of trks
        trkIPSgn: If not all, the sign of the IP to select for the tracks.
                  Useful for the flipped tagger studies

    Outputs:
    	jet_df
        trk_df

    '''


    if nFixedTrks != -1:

        print('Jets before requiring {} tracks: {}'.format(nFixedTrks, len(jet_df.index)))

        trkMult = pd.Series([trks.size for trks in trk_df.sd0_arr])
        nTrksMask = (trkMult == nFixedTrks)

        jet_df = jet_df[nTrksMask]
        jet_df.reset_index(inplace=True,drop=True)

        trk_df = trk_df[nTrksMask]
        trk_df.reset_index(inplace=True,drop=True)

        print('Jets after requiring {} tracks: {}'.format(nFixedTrks, len(jet_df.index)))

    assert trkIPSgn == "all" # Need to refactor this code still

    # if trkIPSgn == 'all':
    #     print('Skipping any trk cuts')
#
    # else:
#
    #     if trkIPSgn == 'pos' or trkIPSgn == 'positive':
    #         print('Selecting trks with positive sd0 and sz0')
#
    #         trkMask = pd.Series([ list( (np.array(row.sd0_arr) > 0.) & (np.array(row.sz0_arr) > 0.) )
    #                              for row in trk_df[['sd0_arr', 'sz0_arr']].itertuples() ])
#
#
    #     elif trkIPSgn == 'neg' or trkIPSgn == 'negative':
    #         print('Selecting trks with negative sd0 and sz0')
#
    #         trkMask = pd.Series([ list( (np.array(row.sd0_arr) < 0.) & (np.array(row.sz0_arr) < 0.) )
    #                              for row in trk_df[['sd0_arr', 'sz0_arr']].itertuples() ])
#
    #     else:
    #         print('Error: {} is NOT a valid option for trkIPSgn. Returning'.format(trkIPSgn))
    #         return
    #     print('Successfully created the trkMask for {} IPs'.format(trkIPSgn))

#        new_trk_df = pd.DataFrame()
#
#        for col in trk_df.columns:
#            print('Applying mask to {}'.format(col))
#            new_trk_df[col] = pd.Series([var[mask] for var, mask in zip(trk_df[col], trkMask)])
#
#        trk_df = new_trk_df

    # I think the df inputs are NOT passed by reference
    return jet_df, trk_df


def pTReweight(jet_df):
    '''
    Do the pT reweighting for the sample, and add a new col "sample_weight" to the jet_df

    '''

    # For consistency, use the same values that Zihao used
    start, stop, step = 0, 3000, 10 # GeV
    pT_edges = np.arange(start, stop+step, step)

    # Get the bin that each entry corresponds to
    x_ind = np.digitize(jet_df.pt_orig, pT_edges) - 1

    # Make the histograms
    l_hist, _ = np.histogram(jet_df[jet_df.LabDr_HadF==0].pt_orig, bins=pT_edges)
    c_hist, _ = np.histogram(jet_df[jet_df.LabDr_HadF==4].pt_orig, bins=pT_edges)
    b_hist, _ = np.histogram(jet_df[jet_df.LabDr_HadF==5].pt_orig, bins=pT_edges)

    # Normalize and add epsilon so that you never get a divide by 0 error
    epsilon = 1e-8
    l_hist = l_hist / np.sum(l_hist) + epsilon
    c_hist = c_hist / np.sum(c_hist) + epsilon
    b_hist = b_hist / np.sum(b_hist) + epsilon

    # Reweight the b-jets to have the pT and eta dist as bkg
    jet_df['sample_weight'] = pd.Series([l_hist[ix] / b_hist[ix] if pdg == 5 else l_hist[ix] / c_hist[ix] if pdg == 4 else 1  \
                                   for pdg, ix in zip(jet_df.LabDr_HadF, x_ind)])



def scale(data, var_names, savevars, filename, mask_value=0):
    '''
    Args:
    -----
        data: a numpy array of shape (nb_events, nb_particles, n_variables)
        var_names: list of keys to be used for the model
        savevars: bool -- True for training, False for testing
                  it decides whether we want to fit on data to find mean and std
                  or if we want to use those stored in the json file
        VAR_FILE: string: Where to save the output
        mask_value: the value to mask when taking the avg and stdev

    Returns:
    --------
        modifies data in place, writes out scaling dictionary

    Reference: Taken from Micky's dataprocessing.py file in
    https://github.com/mickypaganini/RNNIP
    '''
    import json

    scale = {}
    if savevars:
        for v, name in enumerate(var_names):
            print('Scaling feature {} of {} ({}).'.format(v + 1, len(var_names), name))
            f = data[:, :, v]
            slc = f[f != mask_value]
            m, s = slc.mean(), slc.std()
            slc -= m
            slc /= s
            data[:, :, v][f != mask_value] = slc.astype('float32')
            scale[name] = {'mean' : float(m), 'sd' : float(s)}

        with open(filename, 'w') as varfile:
            json.dump(scale, varfile)

    else:
        with open(filename, 'r') as varfile:
            varinfo = json.load(varfile)

        for v, name in enumerate(var_names):
            print('Scaling feature {} of {} ({}).'.format(v + 1, len(var_names), name))
            f = data[:, :, v]
            slc = f[f != mask_value]
            m = varinfo[name]['mean']
            s = varinfo[name]['sd']
            slc -= m
            slc /= s
            data[:, :, v][f != mask_value] = slc.astype('float32')


def prepareForKeras(jet_df, trk_xr, jetTag,
                    noNormStr, logNormStr, jointNormStr, nTrkClasses,
                    subDir='mc16d_Topo_ttbar', sortFlag="sd0_rev", ipTag=""):
    '''
    Prepare the train and test inputs / labels for Keras, and save them as h5py files
    in the "data/" directory.

    NOTE: I also want to put the selection of the variables that I'm using for the network here!

    Inputs:
    - jet_df: The dataframe with the jet level vars
    - trk_xr: The xarray with the trk level vars
    - jetTag: A tag representing how many jets are in the dataset
    - nTrkClasses: The number of track classes to use for the trk truth labels
    - subDir: Folder inside data/ to save the output to
    - sortFlag: The applied track sort (default sd0_rev)
    - norm: Normalization scheme used (default logNorm)
    - ipTag: A tag to append to the datafile for what signage selection was used
             for the files. An empty string uses all of the tracks, while "pos"
             (neg) selects only those with postive (negative) IPs.
    - embed: If true, put the trk grade in the h5py files. Otherwise, use the
             raw variables (i.e, the hits and holes) used to define the trk
             category.

    '''

    assert (nTrkClasses == 4) or (nTrkClasses == 5) # only functionality for these trk labels atm

    # Step 0: Process the string inputs for the vars in each norm sheme -> list
    noNormVars = strToList(noNormStr)
    logNormVars = strToList(logNormStr)
    jointNormVars = strToList(jointNormStr)

    # Step 1: Select the relevant variables
    inpts = noNormVars + logNormVars + jointNormVars

    # Check that all of the requested inputs are actually vars in trk_xr
    trkInputs = list(trk_xr.coords['var'].values)
    for inpt in inpts:
        if inpt not in trkInputs:
            raise ValueError('In prepareForKeras(): requested var {} not in trk_xr'.format(inpt))

    # Keep track of which tracks in the jet are masked
    mask = ~ np.all(trk_xr.values == 0, axis=-1)

    # trk_grade hack
    if 'ip3d_grade' in trkInputs:
        # Since the valid categories are from 0 - 13, need to shift the values
        # so that the embedding layer can mask with 0s
        trk_xr.loc[:,:,'ip3d_grade'] = xr.where(mask, trk_xr.loc[:,:,'ip3d_grade']+1, 0)

    X = trk_xr.loc[:,:,inpts].values
    ix = trk_xr.indexes['jet']
    print("X.shape = ", X.shape)

    # If you're only training with 4 track classes, need to combine the b+c HF
    # tracks from the HF variable
    y_trk = trk_xr.loc[:, :, 'orig'].values
    if nTrkClasses == 4:
        for ti_new, ti_old,  in enumerate([1,2,3,-1]):
            y_trk = np.where(y_trk==ti_old, ti_new, y_trk)
    trkTag = "_{}trkClasses".format(nTrkClasses)

    # Step 2: Train / test split
    random_seed = 25

    pdg_to_class = {0:0, 4:1, 5:2, 15:3}
    y = jet_df.LabDr_HadF.replace(pdg_to_class).values

    X_train, X_test, y_train, y_test, y_trk_train, y_trk_test, ix_train, ix_test, weights_train, weights_test, nTrks_train, nTrks_test = \
        train_test_split(X, y, y_trk, ix, jet_df.sample_weight, jet_df.ip3d_ntrk, test_size=0.5,
                         random_state=random_seed)

    # Step 3: Normalize the continuous inputs (leave the category variables as is)
    # I now have four different options for normalizing the inputs which are selected by the
    # arg parser and then passed into this function
    nTrks = X.shape[1]

    # Take the log of the desired variables
    for i, v in enumerate(logNormVars):
        j = i + len(noNormVars)
        X_train[:,:,j][mask[ix_train]] = np.log(X_train[:,:,j][mask[ix_train]])
        X_test[ :,:,j][mask[ix_test]]  = np.log(X_test[ :,:,j][mask[ix_test]])

    # Get a string representing the variables getting scaled
    varTag = "_".join(noNormVars) if len(noNormVars) != 0 else ''
    varTag += '_logNorm_' + "_".join(logNormVars) if len(logNormVars) != 0 else ''
    varTag += '_norm_' + "_".join(jointNormVars) if len(jointNormVars) != 0 else ''

    # Scale the vars and save the files
    scalingFile = "data/{}/scale_{}_{}{}.json".format(subDir,jetTag,varTag,ipTag)
    print(scalingFile)
    scale(X_train[:,:,len(noNormVars):], logNormVars+jointNormVars, savevars=True,  filename=scalingFile)
    scale(X_test[:,:, len(noNormVars):], logNormVars+jointNormVars, savevars=False, filename=scalingFile)

	# Additionally, outuput the data to the

    # Step 4: Save as h5py files
    outputFile = 'data_{}_{}_{}{}{}.hdf5'.format(jetTag,varTag,sortFlag,ipTag,trkTag)
    print("Saving datasets in {}".format(outputFile))
    f = h5py.File('data/{}/{}'.format(subDir,outputFile), 'w')

    f.create_dataset("X_train", data=X_train)
    f.create_dataset("y_train", data=y_train)
    f.create_dataset("y_trk_train", data=y_trk_train)
    f.create_dataset("ix_train", data=ix_train)
    f.create_dataset("weights_train", data=weights_train)
    f.create_dataset("nTrks_train", data=nTrks_train)

    f.create_dataset("X_test",  data=X_test)
    f.create_dataset("y_test",  data=y_test)
    f.create_dataset("y_trk_test",  data=y_trk_test)
    f.create_dataset("ix_test", data=ix_test)
    f.create_dataset("weights_test", data=weights_test)
    f.create_dataset("nTrks_test", data=nTrks_test)

    f.close()


if __name__ == '__main__':

    from argparse import ArgumentParser

    # Start off just using the same OptionParser arguments that Zihao used.
    p = ArgumentParser()
    p.add_argument('--nMaxTrack','--nTrks', type=int, default='15', dest="nMaxTrack",
                   help="Maximum number of tracks (default 15)")
    p.add_argument('--nFixedTrks', type=int, default='-1', dest="nFixedTrks", help="Only select jets with this many tracks (default -1 => use the nMaxTracks to pad the sequence instead)")
    p.add_argument('--trkIPSign','--trkIPSgn', type=str, default='all', dest="trkIPSgn",
                   help="The sign of the IP to select for the tracks: all (default), pos / positive, or neg / negative")
    p.add_argument('--nJets', type=int, default=3e6, dest="nJets",
                   help="Number of jets to process (default 3m)")

    p.add_argument('--filename', type=str,
                   default="/gpfs/slac/atlas/fs1/d/rafaeltl/public/RNNIP/FTAG_ntups/user.rateixei.mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.AOD..dijetSamplesNominal20180629_Akt4EMTo/user.rateixei.14594351.Akt4EMTo._000*.root",
                   dest="filename",
                   help="Path to and name of the root (or .h5 / .nc) files")

    # The info for which subdir to save the output file to
    p.add_argument("--mc", type=str, default="mc16d", dest="mc",
                   help="mc version: mc16a, mc16d (default), mc16e")
    p.add_argument('--jetCollection', type=str, default='Topo', dest="jetCollection",
                   help="Jet collection to train over: Topo (default) or PFlow")
    p.add_argument('--physicsSample', type=str, default='ttbar', dest="physicsSample",
                   help="the physics sample of interest: can be one of ttbar (default),"
                   +"Zprime_1.5TeV, Zprime_5TeV, hybrid_1.5TeV, hybrid_5TeV")


    p.add_argument('--sortFlag', type=str, dest="sortFlag", default="sd0_rev",
                   help="Sorting configuration for the tracks, default sd0_rev.")
    #p.add_argument('--norm', type=str, default='logNorm', dest="norm",
    #               help="Norm scheme for the trk inputs: noNorm, jointNorm, separateNorm, logNorm (default)")

    p.add_argument('--nTrkClasses', type=int, default=4, dest='nTrkClasses', help="# of trk classes")

    p.add_argument('--noNormVars', type=str, default='sd0,sz0,nNextToInnHits,nInnHits,'\
                   +'nsharedBLHits,nsplitBLHits,nsharedPixHits,nsplitPixHits,nsharedSCTHits',
                   help='Variables not to normalize: Pass -1 for an empty list')
    p.add_argument('--logNormVars', type=str, default='ptfrac, dr',
                   help='Variables to take the log of before normalizing, '\
                   +'default ptfrac, dr: Pass -1 for an empty list')
    p.add_argument('--jointNormVars', type=str, default='nPixHits,nSCTHits',
                   help='Variables to whiten: Pass -1 for an empty list')

    args = p.parse_args()

    subDir = "{}_{}_{}".format(args.mc,args.jetCollection,args.physicsSample)


    # Sanity check that you're running w/ a sensible combination of parameters
    if args.trkIPSgn != 'all' and args.sortFlag == 'sd0_absrev':
        print('WARNING: If trkIPSgn == {}, are you sure you want to run w/ |sd0| sort?')

    # Load in the np arrays from Keras
    if ".root" in args.filename:
        print("Processing data from root files: calling getData() and sav:wweData()")
        jet_list, trk_list, jetVars, trkVars  = getData(filename=args.filename, nJets=args.nJets,
                                                        jetCollection=args.jetCollection,sort_flag=args.sortFlag)
        jet_df, trk_xr  = saveData(jet_list, trk_list, jetVars, trkVars, args.nMaxTrack, subDir, args.sortFlag)
    elif ".h5" in args.filename or ".nc" in args.filename:
        jet_df, trk_xr  = loadData(filename=args.filename,
                                   nJets=args.nJets,
                                   sort_flag=args.sortFlag)

    # Apply any trk cuts
    assert args.nFixedTrks == -1 and args.trkIPSgn == 'all'
    # jet_df, trk_xr = trkSelection(jet_df, trk_xr, args.nFixedTrks, args.trkIPSgn)

    # Get the flattened pT dist
    pTReweight(jet_df)

    # Do the train / test split and save the files
    jetTag = nJetsTag(len(jet_df.index))
    if args.nFixedTrks == -1:
        jetTag += "_{}trks".format(args.nMaxTrack)
    else:
        jetTag += "_ONLY_{}trks".format(args.nFixedTrks)
    ipTag = getIPTag(args.trkIPSgn)

    prepareForKeras(jet_df, trk_xr, jetTag,
                    args.noNormVars, args.logNormVars, args.jointNormVars,
                    args.nTrkClasses,subDir, args.sortFlag,ipTag)
