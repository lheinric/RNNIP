# RNNIP

Repo for the code for training and evaluating the flavor tagging RNN.

## Step 1: Software setup

## Step 2: Data ready inputs

The preprocessing happens in the `root_to_np.py` function, which outputs a file 

### What are the options that I have for running this file?

This script is rather long becuase there is quite a bit of flexibility with 
what parameters you can run with. To see the current functionality, you just run:

`python root_to_np.py --help`

There are two different starting places you have when running the file

1. From the root files (processed from the flavor tagging framework)

The default parameters in this script currently allow you to run over the root files in Rafael's SLAC gpfs space:

`/gpfs/slac/atlas/fs1/d/rafaeltl/public/RNNIP/FTAG_ntups/user.rateixei.mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.AOD..dijetSamplesNominal20180629_Akt4EMTo/`

The data is extracted from the Ntuple using the `uproot` package, and the jet $p_T$, $\eta$, JVT, and e- overlap removal cuts are applied.

Additionally, the desired sort is applied:
**Convention for the sort flag:**
- The variable name is always all lowercase

Finally, the trk variables are truncated / padded to a 

At this point 


2. From previously processed .h5 and .nc files

Since Option #1 is so time consuming (it takes ~30 min to process a file with 200k jets), 



## Step 3: Training networks

`python trainNet.py --help`

The parameter `nEpochs` lets you set the maximum # of epochs for training, but you don't need to use the maximum, because the models are trained using early stopping, i.e, if the validation loss does not improve after 25 epochs, the training is stopped early.

To run jobs by submitting them to the cluster, refer to `submitJobs.sh`. 
The `slacgpu` queue lets you train models (in parallel) on the slac gpu queues!


## Step 4: Making plots

The `Notebooks` folder allows you to load in models, make plots, and document your studies as you go.

Functions for producing common plots are provided in the 
used across many an (such as roc curves,

(using the `load_model` tag in trainNet.py so you can load in the variables using key word arguments) again.


