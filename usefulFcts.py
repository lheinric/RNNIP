from math import log10, floor
import numpy as np

def round_to_1(x):
    return round(x, -int(floor(log10(abs(x)))))

def nJetsTag(nJets):
    '''
    Make a tag that specifies the number of jets you have
    in the data sample, i.e, 5k for 5000.

    Input:
    Njets: the number of jets in the sample

    Output:
    jetTag (string): The number of jet in one sig fig with the
        units appended.
    '''

    if nJets < 1e3:
        jetTag = "{:.0f}".format(round_to_1(nJets))

    elif nJets < 1e6:
        # Appeding "k" to the file name, round to 1 sig fig
        jetTag = "{:.0f}k".format(round_to_1(nJets / 1e3))

    elif nJets < 1e9:
        jetTag = "{:.0f}m".format(round_to_1(nJets / 1e6))
    elif nJets < 1e12:
        jetTag = "{:.0f}g".format(round_to_1(nJets / 1e9))
    else:
        print('Note; you\'re running with more than a trillion events, are you sure this is what you want to do?')
        jetTag = "{:.0f}t".format(round_to_1(nJets / 1e12))

    return(jetTag)


def getIPTag(trkIPSgn):
    '''
    Given the tag you can feed into the argParser for the IP selection, return
    a tag to append to the datafile.

    Input:
    - trkIPSgn (str): Input from argParser

    Output:
    - ipTag (str): tag to append to a filename
     '''

    if trkIPSgn != "all":
        if trkIPSgn == "pos" or trkIPSgn == "positive":
            ipTag = "_posIP"
        elif trkIPSgn == "neg" or trkIPSgn == "negative":
            ipTag = "_negIP"
        else:
            print("ERROR: {} not a valid flag for trkIPSgn. Returning".format(trkIPSgn))
            return
    else:
        ipTag = ""

    return ipTag


def strToList(myStr, delimitter=","):
    '''
    Go from a string of a list of inputs to a list of strings

    Input:
    - myStr: Comma separated list of vars (in trk_xr)
    - delimitter: The delimmiter to split the list by
    Output:
    - myList: Comma separated list of strs

    '''

    if myStr == '-1':
        myList = []
    else:
        myList = myStr.replace(" ","").split(delimitter)

    # To standardize the order of the vars, first take the set of the list
    # you're returning
    #return list(set(myList))
    return myList

def getTrkMask(X, mask_value=0):
    '''
    Given an array for the track inputs, return a mask for the valid tracks.

    Inputs:
    - X: np array of shape (nJets, nTrks, nTrkFeatures)
    - mask_value: value corresponding to masked inputs (default 0)

    Output:
    - trk_mask: mask for valid tracks, of shape (nJets, nTrks)

    '''

    trk_mask = trk_mask = (np.sum(X, axis=-1) != mask_value)
    return trk_mask
